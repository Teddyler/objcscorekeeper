//
//  ViewController.m
//  ObjCScoreKeeper
//
//  Created by Tyler Barnes on 2/2/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _Team1Name.returnKeyType = UIReturnKeyDone;
    _Team1Name.delegate = self;
    _Team2Name.returnKeyType = UIReturnKeyDone;
    _Team2Name.delegate = self;
    
}


- (IBAction)Stepper1Changed:(UIStepper *)sender {
    
    NSString *winner = nil;
    
    self.Score1.text = [NSString stringWithFormat:@"%i", (int)sender.value];
        
    if (_Stepper1.value == 21) {
        winner = _Team1Name.text;
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Winner" message:[NSString stringWithFormat:@"%@", winner] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* resetButton = [UIAlertAction actionWithTitle:@"RESET" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) { [self.Reset sendActionsForControlEvents:UIControlEventTouchUpInside]; }];
        
        [alert addAction:resetButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}


- (IBAction)Stepper2Changed:(UIStepper *)sender {
    
    NSString *winner = nil;
    
    self.Score2.text = [NSString stringWithFormat:@"%i", (int)sender.value];
    
    if (_Stepper2.value == 21){
        winner = _Team2Name.text;
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"WINNER" message:[NSString stringWithFormat:@"%@", winner] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* resetButton = [UIAlertAction actionWithTitle:@"RESET" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) { [self.Reset sendActionsForControlEvents:UIControlEventTouchDown]; }];
        
        [alert addAction:resetButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if(![touch.view isMemberOfClass:[UITextField class]]) {
        [touch.view endEditing:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)ResetScores:(UIButton *)sender {
    self.Score1.text = @"0";
    self.Score2.text = @"0";
    _Stepper1.value = 0;
    _Stepper2.value = 0;
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"New Players?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* resetButton = [UIAlertAction actionWithTitle:@"RESET" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        _Team1Name.text = nil;
        _Team2Name.text = nil;
    }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction: resetButton];
    [alert addAction: cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
