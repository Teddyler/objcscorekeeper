//
//  ViewController.h
//  ObjCScoreKeeper
//
//  Created by Tyler Barnes on 2/2/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UILabel *Score1;
@property (strong, nonatomic) IBOutlet UILabel *Score2;
@property (strong, nonatomic) IBOutlet UIStepper *Stepper1;
@property (strong, nonatomic) IBOutlet UIStepper *Stepper2;
@property (strong, nonatomic) IBOutlet UITextField *Team1Name;
@property (strong, nonatomic) IBOutlet UITextField *Team2Name;
@property (strong, nonatomic) IBOutlet UIButton *Reset;

- (IBAction)ResetScores:(UIButton *)sender;

@end

