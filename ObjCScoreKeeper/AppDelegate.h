//
//  AppDelegate.h
//  ObjCScoreKeeper
//
//  Created by Tyler Barnes on 2/2/17.
//  Copyright © 2017 Tyler Barnes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

